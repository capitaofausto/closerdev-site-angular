import { Component, Inject, PLATFORM_ID, OnInit } from "@angular/core";
import { Angulartics2GoogleAnalytics } from "angulartics2/ga";
import { isPlatformBrowser } from "@angular/common";
import { LoadingService } from "./shared/components/spinner-initial/loading.service";
import { environment } from "../environments/environment";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "closerDev-site";

  constructor(
    angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
    @Inject(PLATFORM_ID) private platformId: Object,
    public loadingService: LoadingService
  ) {
    if (environment.production) {
      angulartics2GoogleAnalytics.startTracking();
    }
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      setInterval(() => {
        this.loadingService.loading = false;
      }, 1500);
    }
  }
}
