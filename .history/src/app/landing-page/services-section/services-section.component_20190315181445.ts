import { Component, OnInit, ElementRef, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-services-section',
  templateUrl: './services-section.component.html',
  styleUrls: ['./services-section.component.scss']
})
export class ServicesSectionComponent implements OnInit {

  constructor(private el :ElementRef, @Inject(PLATFORM_ID) private platformId: Object) { }

  states=['1','2','3','4','5']
  index=0;
  currentState='1';
  componentPosition = this.el.nativeElement.offsetTop
  ngOnInit() {
    this.componentPosition = this.el.nativeElement.offsetTop
    console.log("Services",this.componentPosition);
    /*if (isPlatformBrowser(this.platformId)) {
      setInterval(() => {
        this.currentState = this.states[this.index];
        this.index++;
        if (this.index == this.states.length) {
          this.index = 0;
        }
      }, 1500);
    } */
  }
  public getComponentPosition(){
    return this.el.nativeElement.offsetTop;
  }

  changeTab(num){
    
    if (isPlatformBrowser(this.platformId)) {
      this.currentState = this.states[num];
    }
    console.log(num);
    /*for(let i=1; i<=5; i++){
      var element = document.getElementById(i.toString());
      if(element.classList.contains("selected")){
        element.classList.remove("selected");
      }
    }
    var element = document.getElementById(num);
    element.classList.add("selected");*/
  }

}
