import { Component, OnInit, HostListener, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() onViewComponent:string;
  constructor(public el:ElementRef) { }
  shrink=false;
  show = false;


  @HostListener('window:scroll', ['$event']) 
  doSomething(event) {
    // console.debug("Scroll Event", document.body.scrollTop);
    // see András Szepesházi's comment below
    // console.debug("Scroll Event", window.pageYOffset );
    const componentPosition = this.el.nativeElement.offsetTop
    const scrollPosition = window.pageYOffset

    this.shrink=scrollPosition> 100;
    
  }

  ngOnInit() {

  }

}
