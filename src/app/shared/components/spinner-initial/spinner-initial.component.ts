import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-spinner-initial',
  templateUrl: './spinner-initial.component.html',
  styleUrls: ['./spinner-initial.component.scss']
})
export class SpinnerInitialComponent implements OnInit {
  loadingMessage = ''; 
  message = 'CloserDev';
  timerHandler: any;
  index = 0;
  constructor(@Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
     
      this.type();



    }
  }

  type(): void {
   
    
    this.timerHandler = setTimeout(() => {
      if (this.index == this.message.length) {
        return;
      }
      const element = this.message[this.index];
      this.loadingMessage += element;
      this.index++;
      console.log(this.loadingMessage);
      this.type();
    }, 100);
    

  }

}
