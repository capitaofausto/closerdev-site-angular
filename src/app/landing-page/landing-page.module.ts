import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TechnologiesSectionComponent } from './technologies-section/technologies-section.component';
import { FrameSectionComponent } from './frame-section/frame-section.component';
import { ProfileSectionComponent } from './profile-section/profile-section.component';
import { HeaderComponent } from './header/header.component';
import { LandingPageComponent } from './landing-page.component';
import { ServicesSectionComponent } from './services-section/services-section.component';
import { ContactSectionComponent } from './contact-section/contact-section.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReactiveFormsModule } from '@angular/forms';
import {AlertModule} from 'ngx-bootstrap'

import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { SharedModule } from '../shared/shared.module';
import { ProcessSectionComponent } from './process-section/process-section.component';
import { NavbarComponent } from './navbar/navbar.component';


@NgModule({
  declarations: [
    LandingPageComponent,
    HeaderComponent,
    ProfileSectionComponent,
    FrameSectionComponent,
    ServicesSectionComponent,
    TechnologiesSectionComponent,
    ContactSectionComponent,
    ProcessSectionComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    SharedModule,
    NgxPageScrollCoreModule,
    NgxPageScrollModule,
    AlertModule.forRoot()  
  ]
})
export class LandingPageModule { }
