import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private el:ElementRef) { }
  componentPosition = this.el.nativeElement.offsetTop

  ngOnInit() {
  this.componentPosition = this.el.nativeElement.offsetTop
  // console.log("Header", this.componentPosition);
  }
  public getComponentPosition(){
    return this.el.nativeElement.offsetTop;
  }
  

}
