import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-technologies-section',
  templateUrl: './technologies-section.component.html',
  styleUrls: ['./technologies-section.component.scss']
})
export class TechnologiesSectionComponent implements OnInit {

  constructor(private el: ElementRef) { }
  componentPosition = this.el.nativeElement.offsetTop;
  text:String;
  img_src = "../../../assets/imgs/techs/angular.png";
  img_alt = "CloserDev can provide you Angular development";
  img_title = "Angular";
  dict = {
    "Angular":"Angular is a platform that makes it easy to build applications with the web. Angular combines declarative templates, dependency injection, end to end tooling, and integrated best practices to solve development challenges.",
    "React.js":"React (also known as React.js or ReactJS) is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications.",
    "Node.js":"Node.js is a server-side platform wrapped around the JavaScript language for building scalable, event-driven applications.",
    "MongoDB":"MongoDB is a cross-platform and open-source document-oriented database, a kind of NoSQL database.This makes data integration for certain types of applications faster and easier. MongoDB is built for scalability, high availability and performance from a single server deployment to large and complex multi-site infrastructures.",
    "Docker":"Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.",
    "Java":"Java is a high-level programming language developed by Sun Microsystems. It was originally designed for developing programs for set-top boxes and handheld devices, but later became a popular choice for creating web applications.",
    "Loopback":"A LoopBack model is a JavaScript object with both Node and REST APIs that represents data in backend systems such as databases. Models are connected to backend systems via data sources.",
    "Ionic":"Ionic is a powerful HTML5 SDK that helps you build native-feeling mobile apps using web technologies like HTML, CSS, and Javascript. Ionic is focused mainly on the look and feel, and UI interaction of your app."
  };

  ngOnInit() {
    this.componentPosition = this.el.nativeElement.offsetTop
    this.text=this.dict.Angular;

  }
  public getComponentPosition(){
    return this.el.nativeElement.offsetTop;
  }

  showTech(event){
    var target = event.target || event.srcElement || event.currentTarget;
    console.log(target);
    console.log(target.src);
    if(target.src !== undefined){
      this.img_src = target.src;
      this.img_alt = target.alt;
      this.img_title = target.title;
      this.text = this.dict[target.title];
      console.log(this.dict[target.title]);
    }
  }

}
