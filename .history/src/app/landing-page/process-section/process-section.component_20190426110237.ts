import {
  Component,
  OnInit,
  HostListener,
  ViewChild,
  ElementRef,
  AfterViewInit
} from "@angular/core";
import {
  transition,
  state,
  trigger,
  style,
  animate
} from "@angular/animations";

@Component({
  selector: "app-process-section",
  templateUrl: "./process-section.component.html",
  styleUrls: ["./process-section.component.scss"],
  animations: [
    trigger("element", [
      state(
        "hidden",
        style({
          opacity: "0"
          // transform: 'translateX(-500px)'
        })
      ),
      state(
        "present",
        style({
          opacity: "1"
          // transform: 'translateX(0px)'
        })
      ),
      transition("hidden <=> present", animate(1000))
      // transition('highlighted => normal', animate(800))
    ])
  ]
})
export class ProcessSectionComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    this.componentPosition = this.el.nativeElement.offsetTop;
  }
  public getComponentPosition() {
    return this.el.nativeElement.offsetTop;
  }

  stateNeeds = "hidden";
  stateProposal = "hidden";
  stateCode = "hidden";
  stateLaunch = "hidden";

  positions = {};

  @ViewChild("needs") needs: ElementRef;
  @ViewChild("proposal") proposal: ElementRef;
  @ViewChild("code") code: ElementRef;
  @ViewChild("launch") launch: ElementRef;
  componentPosition = this.el.nativeElement.offsetTop;

  @HostListener("window:scroll", ["$event"])
  doSomething(event) {
    // console.debug("Scroll Event", document.body.scrollTop);
    // see András Szepesházi's comment below
    // console.debug("Scroll Event", window.pageYOffset );
    let componentPosition = this.el.nativeElement.offsetTop;
    const scrollPosition = window.pageYOffset;
    if (
      scrollPosition + 200 >=
      componentPosition + this.needs.nativeElement.offsetTop
    ) {
      this.stateNeeds = "present";
    }
    if (
      scrollPosition + 200 >=
      componentPosition + this.proposal.nativeElement.offsetTop
    ) {
      this.stateProposal = "present";
    }
    if (
      scrollPosition + 200 >=
      componentPosition + this.code.nativeElement.offsetTop
    ) {
      this.stateCode = "present";
    }
    if (
      scrollPosition + 200 >=
      componentPosition + this.launch.nativeElement.offsetTop
    ) {
      this.stateLaunch = "present";
    }
  }

  constructor(public el: ElementRef) {}

  ngOnInit() {}
}
