import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { Angulartics2Module } from 'angulartics2';
import { FooterComponent } from './components/footer/footer.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SpinnerInitialComponent } from './components/spinner-initial/spinner-initial.component';
@NgModule({
  declarations: [
    SpinnerComponent,
    FooterComponent,
    SpinnerInitialComponent
  ],
  imports: [
    CommonModule,
    Angulartics2Module.forRoot(),
    AngularFontAwesomeModule
  ],
  exports:[
    SpinnerComponent,
    Angulartics2Module,
    FooterComponent,
    SpinnerInitialComponent
  ]
})
export class SharedModule { }
