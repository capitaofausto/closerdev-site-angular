import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Email } from './../../shared/models/email';
import { EmailService } from './../../shared/services/email.service';

@Component({
  selector: 'app-contact-section',
  templateUrl: './contact-section.component.html',
  styleUrls: ['./contact-section.component.scss']
})
export class ContactSectionComponent implements OnInit {

  public contactForm: FormGroup
  public messageError: string;
  public sucess: boolean = false;
  public error: boolean = false;
  public waiting: boolean = false;
  componentPosition = this.el.nativeElement.offsetTop

  constructor(private emailService: EmailService, private el: ElementRef) { }

  ngOnInit() {
    this.componentPosition = this.el.nativeElement.offsetTop
    console.log("Contact",this.componentPosition);

    this.contactForm = new FormGroup({
      'userData': new FormGroup({
        'nome': new FormControl(null, [Validators.required]),
        'email': new FormControl(null, [Validators.required, Validators.email])
      }),
      'message': new FormControl("", [Validators.required, Validators.maxLength(500)]),
      'subject': new FormControl(null, [Validators.required]),
      'phone': new FormControl(null, [Validators.pattern("^$|^(0|[1-9][0-9]*)$")])
    });
  }

  public getComponentPosition(){
    return this.el.nativeElement.offsetTop;
  }

  onSubmit() {
    if (this.contactForm.valid) {
      this.waiting = true;
      let email = new Email(this.contactForm.value.message,
        this.contactForm.value.phone,
        this.contactForm.value.userData.email,
        this.contactForm.value.subject,
        this.contactForm.value.userData.nome)
      this.emailService.sendEmail(email).subscribe((data) => {
        this.sucess = true;
        this.waiting = false;
        setTimeout(() => {
          this.sucess = false;
        }, 4000);
      }, (err) => {
        this.waiting = false;
        console.log(err);
        if (err.status == 200) {
          this.sucess = true;
          setTimeout(() => {
            this.sucess = false;
          }, 4000);
        }
        else {
          this.error = true;
          setTimeout(() => {
            this.error = false;
          }, 4000);
        }
      });
      this.contactForm.reset();
    }

  }

}
