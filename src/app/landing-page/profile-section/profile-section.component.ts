import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  Inject,
  PLATFORM_ID
} from "@angular/core";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";
import { isPlatformBrowser } from "@angular/common";

@Component({
  selector: "app-profile-section",
  templateUrl: "./profile-section.component.html",
  styleUrls: ["./profile-section.component.scss"],
  animations: [
    trigger("cardNuno", [
      state(
        "hidden",
        style({
          opacity: "0"
          // transform: 'translateX(-500px)'
        })
      ),
      state(
        "present",
        style({
          opacity: "1"
          // transform: 'translateX(0px)'
        })
      ),
      transition("hidden <=> present", animate(1000))
      // transition('highlighted => normal', animate(800))
    ]),
    trigger("cardManuel", [
      state(
        "hidden",
        style({
          opacity: "0"
          // transform: 'translateX(500px)'
        })
      ),
      state(
        "present",
        style({
          opacity: "1"
          // transform: 'translateX(0px)'
        })
      ),
      transition("hidden <=> present", animate(1000))
      // transition('highlighted => normal', animate(800))
    ]),
    trigger("title", [
      state(
        "hidden",
        style({
          opacity: "0"
        })
      ),
      state(
        "present",
        style({
          opacity: "1"
        })
      ),
      transition("hidden <=> present", animate(1000))
      // transition('highlighted => normal', animate(800))
    ])
  ]
})
export class ProfileSectionComponent implements OnInit {
  constructor(
    public el: ElementRef,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}
  componentPosition = this.el.nativeElement.offsetTop;
  state = "hidden";
  @HostListener("window:scroll", ["$event"])
  doSomething(event) {
    // console.debug("Scroll Event", document.body.scrollTop);
    // see András Szepesházi's comment below
    // console.debug("Scroll Event", window.pageYOffset );

    const scrollPosition = window.pageYOffset;

    if (scrollPosition + 200 >= this.el.nativeElement.offsetTop) {
      this.state = "present";
      this.componentPosition = this.el.nativeElement.offsetTop;
    }
  }
  public getComponentPosition() {
    return this.el.nativeElement.offsetTop;
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.componentPosition = this.el.nativeElement.offsetTop;
      const scrollPosition = window.pageYOffset;
      if (scrollPosition >= this.componentPosition)
        setTimeout(() => {
          this.state = "present";
        }, 500);
    }
  }
}
