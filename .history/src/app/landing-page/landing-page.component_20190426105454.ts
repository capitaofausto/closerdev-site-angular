import { Component, OnInit, HostListener, Renderer2, ViewChild, ElementRef, AfterContentInit, AfterViewInit } from '@angular/core';
import { LoadingService } from '../shared/components/spinner-initial/loading.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  @ViewChild('header') header:any;
  @ViewChild('about') about:any;
  @ViewChild('process') process:any;
  @ViewChild('services') services:any;
  @ViewChild('tech') tech:any;
  @ViewChild('contact') contact:any;
  constructor(public loadingService:LoadingService){}

  components=[
    'header',
    'about',
    'process',
    'services',
    'tech',
    'contact'
  ]
  @HostListener('window:scroll', ['$event']) 
  doSomething(event) {
    // console.debug("Scroll Event", document.body.scrollTop);
    // see András Szepesházi's comment below
    // console.debug("Scroll Event", window.pageYOffset );

    for (let i=this.components.length-1; i >=0; i--) {
      const element = this.components[i];
 
      const componentPosition = this[element].getComponentPosition();
      const scrollPosition = window.pageYOffset
      console.log(scrollPosition);
      
      
      

      if(scrollPosition+0 >= componentPosition){
        console.log(element, this[element]);
        this.onViewComponent=element;
        
        // console.log(element);
        
        break;
      }
      else{
        this.onViewComponent='';
      }
    }
  }
  onViewComponent:string='a';

  ngOnInit() {
  }

}
